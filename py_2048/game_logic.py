# coding=utf-8
"""Module holding the game logic."""

from __future__ import print_function, unicode_literals

import random
from copy import deepcopy


class Py2048Exception(Exception):
    """Exception thrown by the Py2048 game logic."""

    pass


class Py2048(object):
    """Class holding the game logic."""

    def __init__(self, size=4):
        """Init the game table."""
        self._table = []
        self._size = size
        for _ in range(0, size):
            self._table.append([0] * size)
        self._insert_random(size)

    def _get_elem_pos_on_move(self, i, j, move):
        """Get the position of an element based on the move that's made.

        The idea is to change indices based on what move is made rather
        than implement a separate move function for each possible move.
        """
        if move == "left":
            return i, j
        elif move == "right":
            return i, self._size - j - 1
        elif move == "up":
            return j, i
        elif move == "down":
            return self._size - j - 1, i
        raise Py2048Exception("Invalid move direction {}".format(move))

    def _get_elem_on_move(self, i, j, move):
        """Get the value of an element based on the move that's made."""
        pos = self._get_elem_pos_on_move(i, j, move)
        return self._table[pos[0]][pos[1]]

    def _set_elem_on_move(self, i, j, move, from_i, from_j, add=False):
        """Set the position of an element based on the move that's made."""
        # pylint: disable=too-many-arguments
        pos = self._get_elem_pos_on_move(i, j, move)
        mov_pos = self._get_elem_pos_on_move(from_i, from_j, move)
        val = self._table[mov_pos[0]][mov_pos[1]]
        self._table[mov_pos[0]][mov_pos[1]] = 0
        if add:
            val *= 2
        self._table[pos[0]][pos[1]] = val

    def _move(self, move_dir):
        """Perform a movement in the move_dir without inserting new numbers."""
        move_made = False
        for i in range(0, self._size):
            j = -1
            while j < self._size:
                j += 1
                for new_j in range(j + 1, self._size):
                    i_j_value = self._get_elem_on_move(i, j, move_dir)
                    i_new_j_value = self._get_elem_on_move(i, new_j, move_dir)
                    if not i_j_value and i_new_j_value:
                        self._set_elem_on_move(i, j, move_dir, i, new_j)
                        move_made = True
                    elif i_j_value and i_new_j_value == i_j_value:
                        self._set_elem_on_move(i, j, move_dir, i, new_j, True)
                        move_made = True
                        j += 1
                    elif i_new_j_value:
                        break
        return move_made

    def move(self, move_dir="left"):
        """Call _move and insert random numbers after the move is done."""
        if self._move(move_dir):
            self._insert_random()

    def _insert_random(self, max_add=None):
        """
        Add random elements to the game table.

        :param max_add: Maximum number of elements to add.
        :return:
        """
        if max_add is None:
            max_add = self._size / 2
        random.seed()
        to_add = random.randint(1, max_add)
        choices = []  # List of points where we can insert a new element.
        for i in range(0, self._size):
            for j in range(0, self._size):
                if self._table[i][j] == 0:
                    choices.append((i, j))
        added = 0
        max_choices = len(choices)
        while to_add and added < max_choices:
            to_add -= 1
            added += 1
            add_at = random.randint(0, len(choices) - 1)
            add_at = choices.pop(add_at)
            self._table[add_at[0]][add_at[1]] = random.randint(1, 2)

    @property
    def table(self):
        """Return the current state of the game table."""
        return deepcopy(self._table)

    def _can_move_element(self, i, j):
        """Return True if the element can be moved to any direction."""
        val = self._table[i][j]
        if j > 0 and self._table[i][j - 1] in [0, val]:
            return True
        if j < self._size - 1 and self._table[i][j + 1] in [0, val]:
            return True
        if self._can_move_element_up_down(i, j):
            return True
        return False

    def _can_move_element_up_down(self, i, j):
        """Return True if the element can be moved up or down."""
        val = self._table[i][j]
        if i > 0 and self._table[i - 1][j] in [0, val]:
            return True
        if i < self._size - 1 and self._table[i + 1][j] in [0, val]:
            return True
        return False

    def can_move(self):
        """Return True if making a move is possible, False otherwise."""
        for i in range(0, self._size):
            for j in range(1, self._size, 2):
                if not self._table[i][j]:
                    return True
                if self._can_move_element(i, j):
                    return True
        for i in range(1, self._size, 2):
            for j in range(0, self._size, 2):
                if self._can_move_element_up_down(i, j):
                    return True
        return False
