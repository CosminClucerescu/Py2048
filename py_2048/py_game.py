# coding=utf-8
"""Integration into py_game."""
# pylint: disable=no-name-in-module
import sys

import pygame

from pygame import KEYUP, K_DOWN, K_LEFT, K_RIGHT, K_RETURN, K_UP, QUIT
from pygame import init, quit as py_g_quit

from py_2048.game_logic import Py2048

MOVE_TABLE = {
    K_LEFT: "left",
    K_RIGHT: "right",
    K_UP: "up",
    K_DOWN: "down"

}

BLOCK_SIZE = 70


class _ScreenInfo(object):  # pylint: disable=too-few-public-methods
    def __init__(self):
        """Class to hold details about the game screen."""
        self.block_num = 4
        self.draw_font = pygame.font.SysFont(None, 24)
        self.background_color = (0, 0, 0)
        self.text_color = (255, 0, 0)
        self.width = self.block_num * BLOCK_SIZE
        self.height = self.block_num * BLOCK_SIZE


class PyGame2048(object):  # pylint: disable=too-few-public-methods
    """Class for integrating the game logic with pygame."""

    def __init__(self):
        """Setups draw screen details."""
        init()
        self.__game = None
        self.__screen = None
        self._screen_info = _ScreenInfo()

    def _draw_table(self):
        """Draws the game table."""
        self._screen.fill(self._screen_info.background_color)
        x_pos = 0
        for line in self._game.table:
            y_pos = 0
            for number in line:
                text = self._screen_info.draw_font.render(
                    str(number).rjust(15), True,
                    self._screen_info.text_color,
                    self._screen_info.background_color)
                textrect = text.get_rect()
                textrect.topleft = (y_pos, x_pos)
                textrect.width, textrect.height = (BLOCK_SIZE, BLOCK_SIZE)
                self._screen.blit(text, textrect)
                y_pos += BLOCK_SIZE
            x_pos += BLOCK_SIZE

    @property
    def _screen(self):
        """Init the draw area if necessary and returns it."""
        if self.__screen is None:
            width = self._screen_info.width
            height = self._screen_info.height
            self.__screen = pygame.display.set_mode((width, height))
        return self.__screen

    @property
    def _game(self):
        """Init the GameLogic class if necessary and returns it."""
        if self.__game is None:
            self.__game = Py2048()
        return self.__game

    @staticmethod
    def _quit():
        """Quit the game."""
        py_g_quit()
        sys.exit()

    def _game_over_popup(self):
        """Popup the Game Over text."""
        text = self._screen_info.draw_font.render(
            "Game Over, Press Enter to restart. ", True,
            self._screen_info.text_color, self._screen_info.background_color)
        text_rect = text.get_rect()
        text_rect.midbottom = (self._screen_info.height / 2,
                               self._screen_info.width / 2)
        self._screen.blit(text, text_rect)

    def _restart(self):
        """Restart the game."""
        self.__game = Py2048()
        self._draw_table()

    def start(self):
        """Start the game."""
        self._draw_table()
        clock = pygame.time.Clock()
        while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self._quit()
                if event.type == KEYUP:
                    try:
                        self._game.move(MOVE_TABLE[event.key])
                        self._draw_table()
                        if not self._game.can_move():
                            self._game_over_popup()
                        pygame.display.update()
                    except KeyError:
                        if event.key == K_RETURN:
                            self._restart()
            clock.tick(25)


if __name__ == "__main__":
    PyGame2048().start()
