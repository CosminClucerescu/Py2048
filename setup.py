"""Install the py_2048 game."""
from setuptools import setup

setup(
    setup_requires=['pbr'],
    pbr=True,
)
