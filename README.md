# Py2048 [![Build Status](https://travis-ci.org/Cosmin-Clucerescu/Py2048.svg?branch=master)](https://travis-ci.org/Cosmin-Clucerescu/Py2048) [![Coverage Status](https://coveralls.io/repos/github/Cosmin-Clucerescu/Py2048/badge.svg?branch=master)](https://coveralls.io/github/Cosmin-Clucerescu/Py2048?branch=master)
The 2048 game written in Python
