# coding=utf-8
"""Tests for the game_logic module."""
import unittest

import mock

from py_2048.game_logic import Py2048, Py2048Exception

CAN_MOVE_DATA = []


class TestGameLogic(unittest.TestCase):
    """Tests for the game_logic module."""

    def setUp(self):
        """Init the Py2048 class."""
        self._game = Py2048()

    def test_accepted_moves(self):
        """Test the list of accepted moves."""
        self.assertRaises(Py2048Exception, self._game.move, "invalid")
        self._do_all_moves()

    def test_table_property(self):
        """Test the table property."""
        table = self._game.table
        table.append("t")
        self.assertNotEqual(self._game.table, table)

    def test_can_not_move(self):
        """Test the can_move method returns False."""
        for i in range(0, self._game._size):
            for j in range(0, self._game._size):
                self._game._table[i][j] = i + j + 1
        self.assertFalse(self._game.can_move())

    def _do_all_moves(self):
        """Perform the move in all directions."""
        self._game.move("left")
        self._game.move("right")
        self._game.move("up")
        self._game.move("down")

    def test_can_move(self):
        """Test can_move by mimicking play until  the game is over."""
        for _ in range(0, 1000):
            self._game = Py2048()
            moves = 100
            for _ in range(0, moves):
                self._do_all_moves()
                if self._game.can_move() is False:
                    in_table = self._game.table
                    self._game._insert_random = mock.Mock(
                        side_effect=Exception(
                            "Made move after game over\n in_table: {}".format(
                                in_table)))
                    self._do_all_moves()
                    return
            raise Exception(
                "Expected the game to be over in {} moves,"
                " game table: {} ".format(moves, self._game.table))
