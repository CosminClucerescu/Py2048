# coding=utf-8
"""Tests to help with coverage of the can_move method."""
from unittest import TestCase

from py_2048.game_logic import Py2048


class TestCanMove(TestCase):
    """Tests for the can_move method when a move should be possible."""

    def _get_game(self):
        """Get a game with a table with no moves."""
        game = Py2048()
        for i in range(0, game._size):
            for j in range(0, game._size):
                game._table[i][j] = (i + j + 1) * 10
        return game

    def _test_can_move(self, values):
        """Test can_move is true with values as a 2x2 square on the top."""
        for j_start in range(0, 3):
            game = self._get_game()
            count = 0
            for i in range(0, 2):
                for j in range(j_start, j_start + 2):
                    if values[count] != -1:
                        game._table[i][j] = values[count]
                    count += 1
            self.assertTrue(game.can_move())

    def test_can_move_with_zeros(self):
        """Test can_move when zeros are in the matrix."""
        self._test_can_move([0, -1, -1, -1])
        self._test_can_move([-1, 0, -1, -1])
        self._test_can_move([-1, -1, 0, -1])
        self._test_can_move([-1, -1, -1, 0])

    def test_can_move_no_zeros(self):
        """Test can move when no zeros are in the matrix."""
        self._test_can_move([1, 1, -1, -1])
        self._test_can_move([-1, -1, 1, 1])
        self._test_can_move([-1, 1, -1, 1])
        self._test_can_move([1, -1, 1, -1])
