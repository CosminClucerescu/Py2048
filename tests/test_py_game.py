# coding=utf-8
"""Test for the py_game GUI."""
import time
import unittest
import threading

import mock
import pygame

from pygame.locals import KEYUP, K_DOWN, K_LEFT, K_RIGHT, K_RETURN, \
    K_UP, K_a, QUIT
from pygame.event import Event

from py_2048.py_game import PyGame2048


class TestPyGame2048(unittest.TestCase):
    """Tests for the PyGame2048 class."""

    def setUp(self):
        """Init the PyGame2048 class and start it in a separate thread."""
        self._game = PyGame2048()
        self._game_thread = threading.Thread(target=self._game.start)
        self._game_thread.start()

    @mock.patch("py_2048.py_game.PyGame2048._quit")
    def test_quit(self, quit_mock):
        """Test the quit method."""
        pygame.event.post(Event(QUIT, {}))
        self._wait_for_event_processing()
        quit_mock.assert_called()

    @staticmethod
    def _wait_for_event_processing():
        time.sleep(2)

    def tearDown(self):
        """Post a QUIT event and join the game thread."""
        pygame.event.post(Event(QUIT, {}))
        self._wait_for_event_processing()

    @mock.patch("py_2048.py_game.Py2048.move")
    def test_move_down(self, move_mock):
        """Test downward movement."""
        pygame.event.post(Event(KEYUP, {"key": K_DOWN}))
        time.sleep(1)  # Wait a little for the event to be processed
        move_mock.assert_called_with("down")

    @mock.patch("py_2048.py_game.Py2048.move")
    def test_move_up(self, move_mock):
        """Test upward movement."""
        pygame.event.post(Event(KEYUP, {"key": K_UP}))
        time.sleep(1)  # Wait a little for the event to be processed
        move_mock.assert_called_with("up")

    @mock.patch("py_2048.py_game.Py2048.move")
    def test_move_left(self, move_mock):
        """Test movement to the left."""
        pygame.event.post(Event(KEYUP, {"key": K_LEFT}))
        time.sleep(1)  # Wait a little for the event to be processed
        move_mock.assert_called_with("left")

    @mock.patch("py_2048.py_game.Py2048.move")
    def test_move_right(self, move_mock):
        """Test movement to the right."""
        pygame.event.post(Event(KEYUP, {"key": K_RIGHT}))
        time.sleep(1)  # Wait a little for the event to be processed
        move_mock.assert_called_with("right")

    @mock.patch("py_2048.py_game.Py2048.move")
    def test_invalid_key(self, move_mock):
        """Test movement to the right."""
        pygame.event.post(Event(KEYUP, {"key": K_a}))
        time.sleep(1)  # Wait a little for the event to be processed
        move_mock.assert_not_called()

    @mock.patch("py_2048.py_game.Py2048.can_move", return_value=False)
    def test_game_over(self, can_move):
        """Test Game Over popup."""
        pygame.event.post(Event(KEYUP, {"key": K_RIGHT}))
        self._wait_for_event_processing()
        can_move.assert_called_once()

    def test_reset(self):
        """Test the reset game key."""
        game = self._game._game
        pygame.event.post(Event(KEYUP, {"key": K_RETURN}))
        self._wait_for_event_processing()
        self.assertIsNot(game, self._game._game)
