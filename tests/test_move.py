# coding=utf-8
"""Tests movement in the game logic."""
import unittest

from py_2048.game_logic import Py2048

SIZE = 4
ZEROS = [0] * SIZE
TEST_DATA = [([1, 1, 2, 2], [2, 4, 0, 0]),
             ([1, 0, 0, 1], [2, 0, 0, 0]),
             ([2, 2, 2, 2], [4, 4, 0, 0]),
             ([2, 2, 2, 0], [4, 2, 0, 0]),
             ([0, 2, 2, 2], [4, 2, 0, 0]),
             ([1, 2, 2, 1], [1, 4, 1, 0]),
             ([1, 1, 0, 1], [2, 1, 0, 0])
             ]


class TestMoveLeft(unittest.TestCase):
    """Test movement to the left."""

    move_dir = "left"

    def setUp(self):
        """Init the Py2048 instance."""
        self._game = Py2048(SIZE)

    def _assert_move_made(self, start, expected):
        """Assert that moving start to self.move_dir results in expected."""
        game = self._game
        game._table = self._get_table(start)
        game._move(self.move_dir)
        self.assertEqual(game._table, self._get_table(expected),
                         "Input: {}, Expected: {}, Got: {}".format(
                             start, expected, game._table))

    def _get_table(self, line):
        """Get a table that contains the given line only."""
        if self.move_dir in ["left", "right"]:
            if self.move_dir == "right":
                line = list(reversed(line))
            else:
                line = list(line)
            table = [line]
            table.extend([ZEROS] * (SIZE - 1))
            return table
        elif self.move_dir in ["up", "down"]:
            if self.move_dir == "down":
                line = list(reversed(line))
            else:
                line = list(line)
            table = []
            for i in range(0, SIZE):
                new_elem = [line[i]]
                new_elem.extend([0] * (SIZE - 1))
                table.append(new_elem)
            return table
        raise Exception("Shouldn't be here")

    def test_moves(self):
        """Test all moves in TEST_DATA."""
        for start, expected in TEST_DATA:
            self._assert_move_made(start, expected)


class TestMoveRight(TestMoveLeft):
    """Test movement to the right."""

    move_dir = "right"


class TestMoveUp(TestMoveLeft):
    """Test upward movement."""

    move_dir = "up"


class TestMoveDown(TestMoveLeft):
    """Test downward movement."""

    move_dir = "down"
